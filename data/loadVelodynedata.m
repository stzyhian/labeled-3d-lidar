function [velo] = loadVelodynedata (dir1,frame)

dir = [dir1,'/%06d.bin']; 
fid = fopen(sprintf(dir,frame),'rb');
velo = fread(fid,[5 inf],'single')';
fclose(fid);

end
